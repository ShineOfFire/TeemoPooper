require('dotenv').config()

const express = require('express')
const _filter = require('lodash/filter')
const _find = require('lodash/find')
const localtunnel = require('localtunnel')

const app = express()
const path = require('path')
const http = require('http').Server(app)
const io = require('socket.io')(http)

const getColor = require(path.resolve(__dirname + '/src/utils/helpers.js')).getColor
const getUrl = require(path.resolve(__dirname + '/src/middlewares/getUrl.middleware.js'))
// const compressorProject = require(path.resolve(__dirname + '/src/middlewares/compressProject.middleware.js'))

const PORTEXPRESS = parseInt(process.env.PORTEXPRESS);
const PORTSOCKET = parseInt(process.env.PORTSOCKET);
const LOCALTUNNEL = !!parseInt(process.env.LOCALTUNNEL);

app.set('view engine', 'ejs')
app.set('views', path.join(__dirname, '/src/views/'))
app.use(express.static(__dirname + '/assets'))
app.use(express.static(__dirname + '/src'))
app.use(getUrl)
// app.use(compressorProject)

/* --- EXPRESS ROUTING --- */

/**
 * Route '/'
 * link: dynamic insert link css to view (public/styles/)
 */
app.get('/', (req, res) => {
  res.render('home', {
    script: req.routeUrl,
    link: req.routeUrl
  })
})

/**
 * Route 'game'
 * script: dynamic insert script css to view (src/js)
 * link: dynamic insert link css to view (public/styles/)
 */
app.get('/game', (req, res) => {
  res.render('game', {
    script: req.routeUrl,
    link: req.routeUrl,
    socket: true
  })
})

/* --- SOCKET IO --- */

const socketInit = () => {
  let usersList = []
  io.on('connection', (socket) => {
    socket.on('score', (player) => {
      _find(usersList, (o) => o.name === player.name ? o.score = o.score + player.score : o)
      io.emit('setScore', player);
    })

    socket.on('player', (player) => {
      console.info('player : ', player.name, ' is connected');
      if (!player.color) player.color = getColor()

      usersList.push(player)

      io.emit('usersList', usersList);

      socket.on('targetHit', (el) => socket.emit('removeTarget', el))

      socket.on('disconnect', () => {
        console.info('player : ', player.name, ' is disconnect');
        usersList = _filter(usersList, (o) => o.name !== player.name)
        io.emit('usersList', usersList);
        io.emit('quitPlayer', player);
      });
    });
  });
}

let i = 0
const generateTargets = () => {
  let targets = []
  const listsTargets = [
    'Aatrox',
    'Ahri',
    'Akali',
    'Alistar',
    'Amumu',
    'Anivia',
    'Annie',
    'Ashe',
    'AurelionSol',
    'Azir',
    'Bard',
    'Blitzcrank',
    'Brand',
    'Braum',
    'Caitlyn',
    'Camille',
    'Cassiopeia',
    'ChoGath',
    'Corki',
    'Darius',
    'Diana',
    'DrMundo',
    'Draven',
    'Ekko',
    'Elise',
    'Evelynn',
    'Ezreal',
    'Fiddlesticks',
    'Fiora',
    'Fizz',
    'Galio',
    'Gangplank',
    'Garen',
    'Gnar',
    'Gragas',
    'Graves',
    'Hecarim',
    'Heimerdinger',
    'Illaoi',
    'Irelia',
    'Ivern',
    'Janna',
    'JarvanIV',
    'Jax',
    'Jayce',
    'Jhin',
    'Jinx',
    'KaiSa',
    'Kalista',
    'Karma',
    'Karthus',
    'Kassadin',
    'Katarina',
    'Kayle',
    'Kayn',
    'Kennen',
    'KhaZix',
    'Kindred',
    'Kled',
    'KogMaw',
    'LeBlanc',
    'LeeSin',
    'Leona',
    'Lissandra',
    'Lucian',
    'Lulu',
    'Lux',
    'Malphite',
    'Malzahar',
    'Maokai',
    'MasterYi',
    'MissFortune',
    'Mordekaiser',
    'Morgana',
    'Nami',
    'Nasus',
    'Nautilus',
    'Nidalee',
    'Nocturne',
    'Nunu',
    'Olaf',
    'Orianna',
    'Ornn',
    'Pantheon',
    'Poppy',
    'Pyke',
    'Quinn',
    'Rakan',
    'Rammus',
    'RekSai',
    'Renekton',
    'Rengar',
    'Riven',
    'Rumble',
    'Ryze',
    'Sejuani',
    'Shaco',
    'Shen',
    'Shyvana',
    'Singed',
    'Sion',
    'Sivir',
    'Skarner',
    'Sona',
    'Soraka',
    'Swain',
    'Syndra',
    'TahmKench',
    'Taliyah',
    'Talon',
    'Taric',
    'Teemo',
    'Thresh',
    'Tristana',
    'Trundle',
    'Tryndamere',
    'TwistedFate',
    'Twitch',
    'Udyr',
    'Urgot',
    'Varus',
    'Vayne',
    'Veigar',
    'VelKoz',
    'Vi',
    'Viktor',
    'Vladimir',
    'Volibear',
    'Warwick',
    'Wukong',
    'Xayah',
    'Xerath',
    'XinZhao',
    'Yasuo',
    'Yorick',
    'Zac',
    'Zed',
    'Ziggs',
    'Zilean',
    'Zoe',
    'Zyra'
  ]

  const randomTarget = Math.floor(Math.random() * ((listsTargets.length - 1) - 0) + 0)
  const imgRandom = listsTargets[randomTarget]
  const sizeXY = Math.floor(Math.random() * (100 - 20) + 20)

  let TARGETPOINT = Math.floor(Math.random() * (46 - 20) + 20)
  if (sizeXY > 20 && sizeXY <= 45) {
    TARGETPOINT = Math.floor(Math.random() * (100 - 71) + 71)
  } else if (sizeXY > 45 && sizeXY <= 70) {
    TARGETPOINT = Math.floor(Math.random() * (70 - 45) + 45)
  }

  targets.push({
    pos: {
      x: Math.floor(Math.random() * (100 - (sizeXY / 2)) + (sizeXY / 2)),
      y: Math.floor(Math.random() * (200 - 0) + 0)
    },
    sizeXY,
    score: TARGETPOINT,
    imgRandom
  })
  io.emit('generateTargets', targets)
  if (i <= 150) {
    i++
    const time = Math.floor(Math.random() * (7000 - 1500) + 1500)
    setTimeout(generateTargets, time)
  }
}

generateTargets()

socketInit()
if (LOCALTUNNEL) {
  const tunnel = localtunnel(PORTSOCKET, (err, tunnel) => {
    if (err) console.error('Error: Localtunel:', err)
    console.log('Tunnel URL: ', tunnel.url);
    tunnel.url;
  });

  tunnel.on('close', function() {
    if (err) console.log('Localtunel: Closed')
  });
}

http.listen(PORTSOCKET, () => console.info('Socket.io listening on port ' + PORTSOCKET + '!\nLOCALTUNNEL ready :', LOCALTUNNEL))
app.listen(PORTEXPRESS, () => console.info('Game listening on port ' + PORTEXPRESS + '!'))
