const input = document.getElementsByName('name')[0];
const submit = document.getElementsByName('submit')[0];

document.addEventListener("DOMContentLoaded", event => {
	let pseudo;
	input.onkeyup = () => {
		submit.setAttribute('disabled', 'disabled');
		if (input.value.length) {
			pseudo = input.value;
			submit.removeAttribute('disabled');
		}
	};

	submit.onclick = () => {
		if (input.value.length) {
			window.location.href = 'game?' + pseudo;
		}
	};
});