const soundEffectFly = 'assets/sounds/bzzz.mp3';
const musicGame = 'assets/sounds/TEEMIX _ League of Legends Community Collab.mp3';
const soundEffectPoop = 'assets/sounds/caca.mp3';

/* Image generate on click */
const imgCaca = 'assets/images/caca.png';
const imgShroom = 'assets/images/shroom.png';
const imgSuperShroom = 'assets/images/superShroom.png';

const getClientSizeWidth = () => document.querySelector('body').clientWidth;
const getClientSizeHeight = () => document.querySelector('body').clientHeight;
const moucheElement = document.getElementById('mouche');
const teemoSizeWidth = getClientSizeWidth() - moucheElement.clientWidth;

const TweenMax = require('gsap/TweenMax');
const delay = (() => {
	let timer = 0;
	return (callback, ms) => {
		clearTimeout(timer);
		timer = setTimeout(callback, ms);
	};
})();

const initSound = () => {
	const left = moucheElement.style.left = '-150%';
	TweenMax.to(moucheElement, 1.5, { left: 0 });
	// $('#mouche').animate({ left: 0 }, { duration: 1500, queue: false })

	const musicOnGame = new Audio(musicGame);
	const soundFly = new Audio(soundEffectFly);
	const caca = new Audio(soundEffectPoop);

	const volumeMusic = document.getElementById('volumeMusic');
	const volumeEffects = document.getElementById('volumeEffects');
	const volumeMusicSession = parseFloat(localStorage.getItem('volumeMusic'));
	const volumeEffectsSession = parseFloat(localStorage.getItem('volumeEffects'));

	if (localStorage.getItem('volumeMusic')) {
		musicOnGame.volume = volumeMusicSession;
		volumeMusic.value = volumeMusicSession * 10;
		document.getElementById('volumeMusicValue').textContent = volumeMusicSession * 10;
	}

	if (localStorage.getItem('volumeEffects')) {
		soundFly.volume = volumeEffectsSession;
		volumeEffects.value = volumeEffectsSession * 10;
		document.getElementById('volumeEffectsValue').textContent = volumeEffectsSession * 10;
	}

	volumeMusic.onchange = () => {
		localStorage.setItem('volumeMusic', volumeMusic.value / 10);
		musicOnGame.volume = volumeMusic.value / 10;
		document.getElementById('volumeMusicValue').textContent = volumeMusic.value;
	};

	volumeEffects.onchange = () => {
		localStorage.setItem('volumeEffects', volumeEffects.value / 10);
		soundFly.volume = volumeEffects.value / 10;
		document.getElementById('volumeEffectsValue').textContent = volumeEffects.value;
	};

	document.onkeydown = e => {
		if (e.code === 'Space' || e.code === 'Enter') {
			delay(() => {
				caca.play();
				generatePoop();
			}, 250);
		}
	};

	caca.currentTime = 0.8;
	soundFly.currentTime = 0;
	musicOnGame.currentTime = 8;

	caca.volume = volumeEffects.value / 10;
	soundFly.volume = volumeEffects.value / 10;
	musicOnGame.volume = volumeMusic.value / 10;

	soundFly.play();
	musicOnGame.play();

	soundFly.addEventListener('ended', () => {
		soundFly.currentTime = 0;
		soundFly.play();
	}, false);

	musicOnGame.addEventListener('ended', () => {
		musicOnGame.currentTime = 8;
		musicOnGame.play();
	}, false);

	getClientSizeWidth();
	getClientSizeHeight();
};

const gauchedroite = () => {
	const left = moucheElement.style.left == '0px';
	$('#mouche').animate({ left: left ? teemoSizeWidth : 0 }, { duration: 1400, queue: false });
	setTimeout(gauchedroite, 1450);
};

const hautbas = () => {
	const top = $('#mouche').css('top') == '150px';
	$('#mouche').animate({ top: top ? 75 : 150 }, { duration: 250, queue: false });
	setTimeout(hautbas, 300);
};

let i = 0;
const generatePoop = () => {
	const positionTeemo = getPos(moucheElement);
	const poop = document.createElement('img');
	const item = Math.random() * 100;

	poop.className = 'poop' + i++;
	poop.src = imgCaca;

	if (item > 80) poop.src = imgShroom;
	if (item < 10) poop.src = imgSuperShroom;
	poop.style.left = positionTeemo.x + 126 + 'px';
	poop.style.clientWidth = positionTeemo.x;

	const teemoSizeHeight = getClientSizeHeight() - moucheElement.clientHeight;
	const divPoops = document.querySelector('.poops');
	const randomPos = Math.floor(Math.random() * (170 - 130) + 130);
	let ajust = 0;

	if (poop.src.split("/").pop() == 'shroom.png') ajust = 60;
	if (poop.src.split("/").pop() == 'superShroom.png') ajust = 120;
	divPoops.style.top = 50;
	divPoops.appendChild(poop);

	$('.poop' + (i - 1)).animate({
		top: teemoSizeHeight + randomPos,
		zIndex: randomPos + ajust
	}, {
		duration: 2000,
		queue: false
	});
	purgePoop(50);
};

const purgePoop = nb => {
	if (document.getElementsByClassName('poop' + (i - nb))) $('.poop' + (i - nb)).remove();
};

window.addEventListener('resize', () => {
	getClientSizeWidth();
	getClientSizeHeight();
});

document.addEventListener("DOMContentLoaded", event => {
	const namePlayer = getFirstParameterURL(window.location.search);
	document.getElementById('namePlayer').innerHTML = namePlayer;
	socket.emit('namePlayer', namePlayer);

	//socket.on('userConnected', (namePlayer) => {
	//	const span = document.createElement('span');
	//	span.innerHTML = namePlayer + '<br />'
	//	span.id = namePlayer
	//  document.getElementById('listAllUsers').appendChild(span);
	//});

	//socket.on('userDisconnect', (namePlayer) => {
	//	document.getElementById(namePlayer).remove()
	//})

	socket.on('usersList', usersList => {
		document.getElementById('listAllUsers').innerHTML = '';
		usersList.map(name => {
			const span = document.createElement('span');
			span.innerHTML = name + '<br />';
			span.id = name;
			document.getElementById('listAllUsers').appendChild(span);
			document.getElementById('nbUsers').innerHTML = usersList.length;
		});
	});

	gauchedroite();
	hautbas();
	initSound();
});