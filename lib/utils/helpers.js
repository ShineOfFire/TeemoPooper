const getPos = el => {
	for (var lx = 0, ly = 0; el != null; lx += el.offsetLeft, ly += el.offsetTop, el = el.offsetParent);
	return {
		x: lx,
		y: ly
	};
};

const getFirstParameterURL = uri => {
	const url = new URLSearchParams(uri);
	let firstParam;
	if (url.toString().indexOf('&') !== -1) {
		firstParam = url.toString().split('&')[0].slice(0, -1);
	} else {
		firstParam = url.toString().length ? url.toString().slice(0, -1) : 'Anon#' + randomIDHash(5);
	}
	return decodeURI(firstParam);
};

function randomIDHash(sizeId) {
	let text = "";
	const possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

	for (let i = 0; i < sizeId; i++) text += possible.charAt(Math.floor(Math.random() * possible.length));

	return text;
}

const removeFromArray = (array, element) => {
	const index = array.indexOf(element);
	if (index !== -1) array.splice(index, 1);
};