function getUrl(req, res, next) {
  return req.routeUrl = req.originalUrl.split('/')[1] || 'home';
  next();
}

module.exports = getUrl;