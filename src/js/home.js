const input = document.getElementsByName('name')[0];
const submit = document.getElementsByName('submit')[0];
const skin = document.getElementsByClassName('skin')[0];
const left = document.getElementById('left');
const right = document.getElementById('right');
const itemSession = sessionStorage.getItem('fly')

const setSkinStorage = (pos) =>	sessionStorage.setItem('fly', pos * -1)

document.addEventListener('DOMContentLoaded', (event) => {
	let pseudo;
	if (input.value.length) {
		pseudo = input.value
		submit.removeAttribute('disabled')
	}
	input.onkeyup = () => {
		submit.setAttribute('disabled', 'disabled')
		if (input.value.length) {
			pseudo = input.value
			submit.removeAttribute('disabled')
		}
	}

	submit.onclick = () => {
		if (input.value.length) window.location.href = 'game?' + pseudo
	}

	let pos = 0
	if (itemSession !== '0') {
		pos = parseInt(itemSession) * -1 || 0
	}
	setSkinStorage(pos)

	left.onclick = () => {
		pos = pos - 130
		if (pos < -650) pos = 0
		skin.style.backgroundPositionX = pos + 'px';
		setSkinStorage(pos)
	}

	right.onclick = () => {
		pos = pos + 130
		if (pos > 0) pos = -650
		skin.style.backgroundPositionX = pos + 'px';
		setSkinStorage(pos)
	}

	document.onkeydown = (e) => {
		switch (e.code) {
			case 'ArrowLeft':
				pos = pos - 130
				if (pos < -650) pos = 0
				break;
			case 'ArrowRight':
				pos = pos + 130
				if (pos > 0) pos = -650
				break;
		}
		setSkinStorage(pos)
	}
	skin.style.backgroundPositionX = pos + 'px';
});
