let canPlay = false;
const soundEffectFly = 'assets/sounds/bzzz.mp3';
const musicGame = 'assets/sounds/TEEMIX _ League of Legends Community Collab.mp3'
const soundEffectPoop = 'assets/sounds/caca.mp3'
const laughtTeemo1 = 'assets/sounds/laughtTeemo1.mp3'
const laughtTeemo2 = 'assets/sounds/laughtTeemo2.mp3'
const laughtTeemo3 = 'assets/sounds/laughtTeemo3.mp3'
const explosion = 'assets/sounds/explosion.mp3'

const musicOnGame = new Audio(musicGame)
const soundFly = new Audio(soundEffectFly)
const LaughtTeemo1 = new Audio(laughtTeemo1)
const LaughtTeemo2 = new Audio(laughtTeemo2)
const LaughtTeemo3 = new Audio(laughtTeemo3)

const volumeMusic = document.getElementById('volumeMusic')
const volumeEffects = document.getElementById('volumeEffects')
const volumeMusicSession = parseFloat(localStorage.getItem('volumeMusic'))
const volumeEffectsSession = parseFloat(localStorage.getItem('volumeEffects'))

/* Image generate on click */
const imgCaca = 'assets/images/caca.gif'
const imgShroom = 'assets/images/shroom.gif'
const imgSuperShroom = 'assets/images/superShroom.png'

/* Name current player */
const currentPlayer = document.getElementsByClassName('namePlayer')[0]
const currentPlayerImage = document.getElementById('mouche')

const namePlayer = getFirstParameterURL(window.location.search)
const skin = sessionStorage.getItem('fly') || 0

const getClientSizeWidth = () => document.querySelector('body').clientWidth
const getClientSizeHeight = () => document.querySelector('body').clientHeight
const playerElement = document.getElementsByClassName('player')[0]

const teemoSizeWidth = getClientSizeWidth() - playerElement.clientWidth

const delay = (
	() => {
	  let timer = 0;
	  return (callback, ms) => {
	    clearTimeout (timer);
	    timer = setTimeout(callback, ms);
	  };
	})
();

const initSound = () => {
	if (localStorage.getItem('volumeMusic')) {
		musicOnGame.volume = volumeMusicSession
		volumeMusic.value = volumeMusicSession * 10
		document.getElementById('volumeMusicValue').textContent = volumeMusicSession * 10
	}

	if (localStorage.getItem('volumeEffects')) {
		soundFly.volume = volumeEffectsSession
		volumeEffects.value = volumeEffectsSession * 10
		document.getElementById('volumeEffectsValue').textContent = volumeEffectsSession * 10
	}

	volumeMusic.onchange = () => {
		localStorage.setItem('volumeMusic', volumeMusic.value / 10)
		musicOnGame.volume = volumeMusic.value / 10
		document.getElementById('volumeMusicValue').textContent = volumeMusic.value
	}

	volumeEffects.onchange = () => {
		localStorage.setItem('volumeEffects', volumeEffects.value / 10)
		soundFly.volume = LaughtTeemo1.volume =	LaughtTeemo2.volume =	LaughtTeemo3.volume =	volumeEffects.value / 10
		document.getElementById('volumeEffectsValue').textContent = volumeEffects.value
	}

	const teemoRandomSound = () => {
		const ran = Math.floor(Math.random() * (1 - 4) + 4);
		switch (ran) {
			case 1:
				LaughtTeemo1.play()
				break;
			case 2:
				LaughtTeemo2.play()
				break;
			case 3:
				LaughtTeemo3.play()
				break;
		}
	}

	LaughtTeemo1.currentTime = 0
	LaughtTeemo2.currentTime = 0
	LaughtTeemo3.currentTime = 0
	soundFly.currentTime = 0
	musicOnGame.currentTime = 8

	LaughtTeemo1.volume = volumeEffects.value / 10
	LaughtTeemo2.volume = volumeEffects.value / 10
	LaughtTeemo3.volume = volumeEffects.value / 10
	soundFly.volume = volumeEffects.value / 10
	musicOnGame.volume = volumeMusic.value / 10

	soundFly.play()
	musicOnGame.play()

	soundFly.addEventListener('ended', () => {
		soundFly.currentTime = 0
		soundFly.play()
	}, false)

	musicOnGame.addEventListener('ended', () => {
		musicOnGame.currentTime = 8
		musicOnGame.play()
	}, false)

	playerElement.onclick = (e) => teemoRandomSound()

	let pressed = false
	window.onkeypress = (e) => {
		if (pressed) return false
		if (canPlay && (e.keyCode === 32 || e.keyCode === 13)) {
			console.log("dsds");
			const caca = new Audio(soundEffectPoop)
			caca.currentTime = 0.8
			caca.volume = volumeEffects.value / 10
			caca.play()

			generatePoop()
    	pressed = true
      setTimeout(() => pressed = false, 350);
		}
	}

	getClientSizeWidth()
	getClientSizeHeight()
}

const gauchedroite = (el) => {
	const delayByWindow = Math.round((getClientSizeWidth() / 1000) * 100) / 100
	const x = el.getBoundingClientRect().x
	const moveXAnimation = TweenLite.to(el, delayByWindow, {
		x: x === 0 ? getClientSizeWidth() - el.clientWidth : 0
	});

	moveXAnimation.eventCallback('onComplete', () => gauchedroite(el))
}

const hautbas = (el) => {
	const topValue = parseInt(el.style.top.split('px')[0])
	const moveYAnimation = TweenLite.to(el, 0.3, {
		y: topValue > 150
			? topValue - Math.floor(Math.random() * (topValue - 50) + 50)
			: topValue + Math.floor(Math.random() * (10 - topValue) + topValue)
	});
	moveYAnimation.eventCallback('onComplete', () => hautbas(el))
}

const collision = (poop) => {
	const targets = document.querySelector('#targets').children
	let i = targets.length
	const poopNb = document.getElementsByClassName(poop.className.split(' ')[1])[0]

	while (--i > -1) {
		if (Draggable.hitTest(poop, targets[i])) {
			const Explosion = new Audio(explosion)
			Explosion.currentTime = 0
			Explosion.volume = volumeEffects.value / 10
			Explosion.play()

			const prevScore = document.getElementById(namePlayer).children[2].innerText
			socket.emit('score', {
				name: namePlayer,
				score: parseInt(prevScore) + targets[i].score
			})
			poopNb.remove()

			socket.emit('targetHit', {
				elementClass: targets[i].className,
				playerHit: currentPlayer.innerText
			})
		}
	}
}

let i = 0
const generatePoop = () => {
	const positionTeemo = playerElement.getBoundingClientRect()
	const imgPoop = document.createElement('img')
	const item = Math.random() * 100

	imgPoop.className = 'imgPoop poop'+ i++
	imgPoop.src = imgCaca

	if (item > 80) imgPoop.src = imgShroom
	if (item < 10) imgPoop.src = imgSuperShroom
	imgPoop.style.left = positionTeemo.x + 50 + 'px'
	imgPoop.style.clientWidth = positionTeemo.x

	const teemoSizeHeight = getClientSizeHeight() - playerElement.clientHeight
	const divPoops = document.querySelector('.poops')

	const randomPos = Math.floor(Math.random() * (150 - 25) + 25)

	let ajust = 0
	// if (imgPoop.src.split("/").pop() == 'shroom.gif') ajust = 10
	if (imgPoop.src.split("/").pop() == 'superShroom.png') ajust = 25
	divPoops.style.top = 50
	divPoops.appendChild(imgPoop)

	imgPoop.style.width = '55px'
	if (randomPos >= 100) imgPoop.style.width = '35px'
	if (randomPos >= 50) imgPoop.style.width = '45px'

	const poop = document.getElementsByClassName('poop' + (i-1))[0]

	TweenLite.to(poop, 1, {
		y: teemoSizeHeight - randomPos,
		zIndex: (teemoSizeHeight - randomPos) + ajust,
	  onUpdate: () => collision(poop)
	})
	purgePoop(10)
}

const purgePoop = (nb) => {
	const poopNb = document.getElementsByClassName('poop'+ (i-nb))[0]
	if (poopNb) {
		const poopAnimation = TweenLite.to(poopNb, 1.5, { opacity: 0 });
		poopAnimation.eventCallback('onComplete', () => poopNb.remove())
	}
}

let j = 0
const purgeTarget = (nb) => {
	const targetNb = document.getElementsByClassName('target'+ (j-nb))[0]
	if (targetNb) {
		const poopAnimation = TweenLite.to(targetNb, 1.5, { opacity: 0 });
		poopAnimation.eventCallback('onComplete', () => targetNb.remove())
	}
}

window.addEventListener('resize', () => {
	getClientSizeWidth()
	getClientSizeHeight()
})

document.addEventListener('DOMContentLoaded', (event) => {
	playerElement.style.top = (Math.floor(Math.random() * (250 - 140)) + 140) + 'px';
	const initAnimation = TweenLite.fromTo(playerElement, 2, { x: -450 }, { x: 10 });
	initSound()

	currentPlayerImage.style.backgroundPositionX = skin * -1 + 'px';
	currentPlayer.innerHTML = namePlayer.substr(0, 10)

	socket.emit('player', {
		name: namePlayer,
		score: 0,
		skin
	})

	socket.on('removeTarget', (el) => {
		const targetNb = document.getElementsByClassName(el.elementClass)[0]
		if (targetNb) {
			targetNb.style.border = 'none'
			targetNb.style.boxShadow = 'none'
			targetNb.style.background = 'none'
			targetNb.children[0].style.opacity = 0
			targetNb.children[0].src = 'assets/images/explosion.gif'

			const targetAnimation = TweenLite.fromTo(targetNb.children[1], 1.5, { opacity: 1 }, { opacity: 0 });
			targetAnimation.eventCallback('onComplete', () => targetNb.remove())
		}
	})

	const otherPlayersElement = document.getElementById('otherPlayers')

  socket.on('setScore', (player) => {
  	const divPlayerScore = document.getElementById(player.name).children[2]
  	divPlayerScore.innerHTML = player.score
  	if (player.score <= 2000) {
  		divPlayerScore.style.color = '#16a085'
  	} else if (player.score <= 3000) {
  		divPlayerScore.style.color = '#2ecc71'
  	} else if (player.score <= 4000) {
  		divPlayerScore.style.color = '#27ae60'
  	} else if (player.score <= 5000) {
  		divPlayerScore.style.color = '#3498db'
  	} else if (player.score <= 10000) {
  		divPlayerScore.style.color = '#2980b9'
  	} else if (player.score <= 15000) {
  		divPlayerScore.style.color = '#9b59b6'
  	} else if (player.score <= 20000) {
  		divPlayerScore.style.color = '#8e44ad'
  	} else if (player.score <= 30000) {
  		divPlayerScore.style.color = '#f1c40f'
  	} else if (player.score <= 50000) {
  		divPlayerScore.style.color = '#f39c12'
  	} else if (player.score <= 100000) {
  		divPlayerScore.style.color = '#e67e22'
  	} else if (player.score <= 150000) {
  		divPlayerScore.style.color = '#d35400'
  	} else if (player.score <= 200000) {
  		divPlayerScore.style.color = '#e74c3c'
  	}
  })

  socket.on('usersList', (usersList) => {
  	const selfPlayerFromServer = usersList.find(o => o.name === namePlayer ? o.color : false)
		document.getElementById('listAllUsers').innerHTML = ''
		currentPlayer.style.background = selfPlayerFromServer.color

  	usersList.map((playerOnline) => {
			const playerOnlineElement = document.getElementsByClassName(playerOnline.name)
  		playerElement.style.top = (Math.floor(Math.random() * (250 - 140)) + 140) + 'px';
			const initOnlinePlayersAnimation = TweenLite.fromTo(playerOnlineElement, 2, { x: -450 }, { x: 10 });

			if (usersList.length >= 2 && namePlayer !== playerOnline.name) {
				if (playerOnlineElement.length) playerOnlineElement[0].remove()
				const divContainer = document.createElement('div');
				const divInner = document.createElement('div');
				const div = document.createElement('div');

				divContainer.className = 'player ' + playerOnline.name
				divContainer.style.top = '70px'
				divInner.className = 'namePlayer'
				divInner.innerHTML = playerOnline.name

				div.className = 'mouches'
				div.style.backgroundPositionX = playerOnline.skin * -1 + 'px';

				divInner.style.background = playerOnline.color

				otherPlayersElement.appendChild(divContainer)
				divContainer.appendChild(divInner).appendChild(div)
				divInner.after(div)

				initOnlinePlayersAnimation.eventCallback('onComplete', () => {
					hautbas(divContainer)
					gauchedroite(divContainer)
			  })
			}

  		const lineNamePlayer = document.createElement('div');
			lineNamePlayer.innerHTML = `
				<div
					class="colorPinPlayer"
					style="background:${playerOnline.color}"></div>
				<span>${playerOnline.name.substr(0, 10)}</span>
				<span id="score">${playerOnline.score || 0}</span>
			`
			lineNamePlayer.id = playerOnline.name
			document.getElementById('listAllUsers').appendChild(lineNamePlayer)
  		document.getElementById('nbUsers').innerHTML = usersList.length;
  	})
  })

	socket.on('quitPlayer', (namePlayerGetDisconnect) => {
		const playerElement = document.getElementsByClassName(namePlayerGetDisconnect.name)[0]
		const img = document.createElement('img')
		img.src = 'assets/images/disparition.gif'
		img.style.position = 'absolute'
		img.style.top = '30px'
		img.style.margin = '0 auto'
		TweenLite.to(playerElement.children[1], 0.5, { opacity: 0 });
		playerElement.appendChild(img)
		setTimeout(() => playerElement.remove(), 1400);
	})

	socket.on('generateTargets', (targetsFromServer) => {
		let TARGETPOINT = 0
		const targets = document.getElementById('targets')
		
  	targetsFromServer.map((itemTarget) => {
			const blocTarget = document.createElement('div');
			const imgTarget = document.createElement('img');
			const imgTargetExplosion = document.createElement('img');
			document.createElement('div').innerText = parseInt(TARGETPOINT / 10)

			blocTarget.className = 'target' + j++
			blocTarget.score = itemTarget.score
			blocTarget.style.width = blocTarget.style.height = itemTarget.sizeXY + 'px'
			imgTarget.className = 'target'
			imgTarget.src = 'assets/images/targets/' + itemTarget.imgRandom + '.png'
			imgTargetExplosion.className = 'explosion'
			imgTargetExplosion.src = 'assets/images/explosion.gif'

			TARGETPOINT = Math.floor(Math.random() * (46 - 20) + 20)
			if (itemTarget.sizeXY > 20 && itemTarget.sizeXY <= 45) {
				TARGETPOINT = Math.floor(Math.random() * (100 - 71) + 71)
				blocTarget.style.borderColor = '#e74c3c'
				blocTarget.style.boxShadow = '0 5px #a14203'
			} else if (itemTarget.sizeXY > 45 && itemTarget.sizeXY <= 70) {
				TARGETPOINT = Math.floor(Math.random() * (70 - 45) + 45)
				blocTarget.style.borderColor = '#f1c40f'
				blocTarget.style.boxShadow = '0 8px #784a00'
			}

			const posYTarget = itemTarget.pos.y
			const X = !itemTarget.pos.x - (itemTarget.sizeXY / 2) <= 0
				? itemTarget.pos.x - (itemTarget.sizeXY / 2)
				: 0
			blocTarget.style.left = X + 'vw'
			blocTarget.style.top = '50px'
			targets.appendChild(blocTarget)
			blocTarget.appendChild(imgTarget)
			blocTarget.appendChild(imgTargetExplosion)
		})

		purgeTarget(8)
	})

  initAnimation.eventCallback('onComplete', () => {
  	canPlay = true
		gauchedroite(playerElement)
		hautbas(playerElement)
  })
})
