const getUrl = (req, res, next) => {
  req.routeUrl = req.originalUrl.split('/').pop().split('?').shift() || 'home'
  next()
}

module.exports = getUrl
