const isNode = typeof window === 'undefined'

const getPos = el => {
	for (
		var lx=0, ly=0;
		el != null;
		lx += el.offsetLeft, ly += el.offsetTop, el = el.offsetParent
	);
	return {
		x: lx,
		y: ly
	};
};

const getFirstParameterURL = (uri) => {
	const url = new URLSearchParams(uri);
	let firstParam;
	if (url.toString().indexOf('&') !== -1) {
		firstParam = url.toString().split('&')[0].slice(0, -1)
	} else {
		firstParam = url.toString().length ? url.toString().slice(0, -1) : 'Anon#' + randomIDHash(5)
	}
	return decodeURI(firstParam)
}

const randomIDHash = (sizeId) => {
  let text = "";
  const possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

  for (let i = 0; i < sizeId; i++)
    text += possible.charAt(Math.floor(Math.random() * possible.length));

  console.log("text", text);
  return text;
}

const removeFromArray = (array, element) => {
  const index = array.indexOf(element);
  if (index !== -1) array.splice(index, 1);
}

const getColor = () => ('#' + Math.random().toString(16).slice(2, 8))

const getSkinLists = () => {
	return skinColor = {
		130: 'pinkfly',
		260: 'violetfly',
		390: 'bluefly',
		520: 'caterpillar',
		650: 'ladybug',
		0: 'default'
	}
}

if (isNode) {
	module.exports = {
		getSkinLists,
		getColor,
		getFirstParameterURL,
		getPos,
		randomIDHash,
		removeFromArray
	}
}
